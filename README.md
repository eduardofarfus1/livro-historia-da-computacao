# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Historia do disco rígido](https://gitlab.com/eduardofarfus1/livro-historia-da-computacao/-/blob/master/Capítulos/Historia_Disco_Rígido.md)
1. [Inteligencia Artificial](https://gitlab.com/eduardofarfus1/livro-historia-da-computacao/-/blob/master/Capítulos/Inteligencia_Artificial.md)
1. [Influência Tecnológica na Sociedade e Psicologia](https://gitlab.com/eduardofarfus1/livro-historia-da-computacao/-/blob/master/Capítulos/O%20impacto%20da%20tecnologia%20na%20psicologia.md)
1. [Data Mining](https://gitlab.com/eduardofarfus1/livro-historia-da-computacao/-/blob/master/Capítulos/Mineracao_de_Dados.md)
1. [Criptomoedas e a Nova Economia](https://gitlab.com/eduardofarfus1/livro-historia-da-computacao/-/blob/master/Capítulos/Mineracao_de_Dados.md)


## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11526545/avatar.png?width=90)  | Eduardo Farfus   | eduardofarfus1  | [eduardofarfus@alunos.utfpr.edu.br](mailto:eduardofarfus@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11467782/avatar.png?width=90)  | João Pedro | joao-domingos   | [joaodomingos@alunos.utfpr.edu.br](mailto:joaodomingos@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11521036/avatar.png?width=90)  | Gabriel Alovisi  | Alovisis | [gabriel.alovisi1@gmail.com](mailto:gabriel.alovisi1@gmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11492692/avatar.png?width=90)  | Matheus Avila  | matheusavila | [matheusavila@alunos.utfpr.edu.br](mailto:matheusavila@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11527017/avatar.png?width=90)  | Matheus Lima  | matheusichiro |[matheusichiro@alunos.utfpr.edu.br](mailto:matheusichiro@alunos.utfpr.edu.br) 

