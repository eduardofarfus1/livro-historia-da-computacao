# Inteligência Artificial <h>
Para realizar uma tarefa podemos usar um algoritmo para decidir se é uma boa idéia sair para jogar tênis baseando-se em fatores climáticos ou até interpretar frases e reconhecer imagens de cérebros humanos, conseguimos resolver problemas, relacionar idéias por meio do raciocínio e aprender a partir da experiência.
programar algoritmos para que computadores façam essas coisas têm se mostrado um grande desafio, o foco de muitos pesquisadores têm sido desenvolver conjunto de algoritmos ou sistemas que realize tarefas como reconhecer imagens, mover objetos e aprender informações do cérebro humano ainda são metas distantes pode ser que dentro de 50  a 100 anos já existam computadores e robôs com capacidade gerais muito próximas dos humanos.

É difícil prever como as coisas vão acontecer, o que é mais fácil prever é que essa tecnologia deve cada vez mais beneficiar a sociedade. Essas máquinas nos ajudam a viver vidas melhores, máquinas inteligentes podem nos auxiliar em diagnósticos médicos, cirurgias, resgates, explorações espaciais, e até na psicoterapia. Já existem algoritmos e robôs nos ajudando em todas essas áreas atualmente e a tendência é que só nos ajudem mais conforme consigam fazer coisas mais complicadas. Muitos especialistas acreditam que boa parte das ocupações humanas serão substituídos por computadores e robôs nas próximas décadas.
No brasil entre 40 a 50 mil pessoas morrem por ano em acidentes de trânsito. carros automáticos poderiam ser muito mais seguros e evitar milhares de mortes, apesar de muito se debater sobre o que nos aguarda no futuro, o desenvolvimento e os riscos advindos dessa tecnologia ainda são muito incertos a cada novo avanço feito. 
Na área de reconhecimento de imagens por exemplo pode ser muito mais difícil superar os novos desafios advindos do programa anterior, tarefas que envolvam a tomada de decisão imediata ainda dependem de um ser humano no final das contas, já que parece difícil programar sistemas capazes de tomar decisões baseadas na ética humana, algoritmos podem ser usados para o bem ou para o mal o problema como de costume não está nas ferramentas que desenvolvemos mas sim em como as usamos os algoritmos. A inteligência artificial veio para ficar e isso já parece inevitável se as ações deles vão respeitar a ética humana é algo que agora está em nossas mãos.

# Referências <h>

O que é Inteligência Artificial (IA)? Disponível em: <https://www.oracle.com/br/artificial-intelligence/what-is-ai/>. Acesso em: 7 jun 2022.

‌AURÉLIO, Marco. I.A - Inteligência Artificial. Disponível em: <https://brasilescola.uol.com.br/informatica/inteligencia-artificial.htm>. Acesso em: 7 jun 2022.

# Imagens <h>

![](https://nativeip.com.br/wp-content/uploads/2021/01/inteligencia-artificial-mudando-forma-comunicacao-native.jpg)


![](https://nativeip.com.br/inteligencia-artificial-comunicacao-organizacoes/)
