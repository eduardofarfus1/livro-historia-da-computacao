# Historia do Disco Rígido <h>
Nos dias atuais tudo acaba se tornando uma correria, e são tantas informações que passam pelas pessoas durante o dia. Para facilitar o processo de trabalho, memorização de arquivos e outras funcionalidades o computador foi criado e junto com ele temos o disco rígido, mais conhecido como HD, é uma parte eletrônica com operação mecânica. Por ser um dispositivo não volátil, suas informações não são excluídas após desligamento, sendo assim, sua função é armazenar dados, que entre eles são os programas, documentos e o próprio sistema operacional utilizado pela máquina.
 Rey Johnson, pioneiro da computação nos Estados Unidos foi o criador do primeiro disco rígido. RAMAC 305, criado no ano de 1956 a pedido da Força Aérea dos EUA, foi o primeiro computador a usar discos de armazenamento. O disco IBM 350 exercia capacidade de armazenamento de 5 MB e tinha 50 discos magnéticos de 24 polegadas, pesando aproximadamente uma tonelada. Porém o mesmo não foi posto a venda. 17 anos após, a IBM criou o Winchester 3340, um HD com armazenamento de 10 MB, custando uma média de dois mil dólares.

*A cada 1024 MB (megabytes) temos 1 GB. 

*1024 pode parecer um número estranho, toda via as unidades fazem uso de códigos binários para a gravação 

*das informações, ou seja, sempre sendo múltiplo de 2.

 Em 1997, quando a os componentes não acompanhavam mais a evolução dos computadores a Seagate lançou seu HD com 7.200rpm de velocidade e oito anos após lançaram o Cheetah X15 com 15.000rpm (que são usados por servidores empresariais).
 Várias tecnologias foram introduzidas nesse meio, como IDE, SCSI, RAID, EIDE e a SATA que é utilizada até nos dias de hoje. SATA, introduzido no ano de 2000, para substituir os cabos de fita PATA, é um IDE padrão utilizado na conexão de discos rígidos e drives ópticos à placa-mãe. 
 Atualmente os SSD´s estão sendo introduzidos no mercado no lugar dos HD´s por conta de sua velocidade de processamento, porém essa tecnologia já existe desde a década de 80. O modo de funcionamento é diferente pois o SSD realiza a gravação dos arquivos em um ou vários chips de memória.
 Com isso, é notável a evolução nos avanços dos discos rígidos e também que estão longes do fim. Porém deve-se ter o cuidado necessário pois nem tudo são flores, toda segurança e garantia é necessária no quesito de proteção de dados. Caso exista alguma falha no dispositivo tudo que foi gravado pode ser perdido, o que não é algo bom, sempre são necessárias revisões periódicas nos componentes eletrônicos para manter sua performance.

 # Referências <h>

 https://www.infoescola.com/informatica/disco-rigido/#:~:text=O%20primeiro%20HD%20Surgiu%20em,custa%20menos%20de%2080%20dólares.
 
https://pessoatech.com.br/hd/
https://bot.rec.br/evolucao-do-hd/
https://blog.sigecloud.com.br/historia-do-disco-rigido/
https://www.youtube.com/watch?v=qoi-hCdb4E8
https://www.youtube.com/watch?v=QoD_mmx-8nc
https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjW7YrDz6v4AhWDJrkGHTI_CWMQwqsBegQIHBAB&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DSy3FGM4M5aM&usg=AOvVaw2Dh9TAcrbg8NKIrctxiyZm




