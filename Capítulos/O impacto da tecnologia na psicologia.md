# Os benefícios da tecnologia <h>
Os benefícios da tecnologia têm sido percebidos nos diversos setores da sociedade, tais como a área da saúde, educação, economia, nas empresas públicas, privadas e no terceiro setor.

Hoje em dia é difícil ter uma boa saúde mental, mas o avanço da tecnologia proporcionou uma grande opção de tratamentos de qualidade e sem sair de casa. Vejamos a seguir.

# Tratamento de fobias <h>
As fobias são assuntos constantes nos consultórios psicológicos. Cada vez mais pessoas procuram um atendimento qualificado para aprender a lidar com suas ansiedades extremas. Com a tecnologia, você pode lançar mão de aplicativos, óculos de realidade virtual e outros simuladores, como o EMDR (Dessensibilização e Reprocessamento por Meio dos Movimentos Oculares) que aproximam a pessoa do seu medo.

Nesse sentido, ele consegue visualizar o seu problema e enfrentá-lo dentro de um ambiente seguro e controlado, no qual pode sair da simulação a qualquer momento e conversar com você sobre o que sentiu e como reagiu — física e psicologicamente — aos estímulos apresentados.

Isso permite uma interação maior entre você e ele, além de otimizar o seu avanço na psicoterapia. É comum que, em menos tempo, o cliente já se sinta mais confortável em enfrentar seus medos e desenvolva a coragem para lidar com ele fora das simulações.

# Estimulação cognitiva <h>
Indo mais além, também é possível estimular pacientes que apresentam alguma dificuldade cognitiva. Antes da tecnologia, esse processo poderia ser feito por meio de jogos físicos e testes psicológicos, além das intervenções verbais e medicamentosas — nos casos mais graves, desde que com acompanhamento médico.

Hoje, é possível utilizar diversos equipamentos de monitoramento cognitivo alinhado às técnicas psicológicas para auxiliar no controle e estímulo cognitivo. Assim, você consegue identificar as melhores formas de dar continuidade ao tratamento, bem como percebe as possíveis interações multiprofissionais que podem ser feitas.

# Utilização de jogos <h>
Você lembra que comentamos que a realidade virtual é uma excelente aliada da Psicologia? Pois é, além dela existem diversos jogos que otimizam o seu trabalho, oferecem mais autonomia, é claro, aprimoram o vínculo terapêutico. Assim, é possível ir além do tratamento de fobia, utilizando essa alternativa para auxiliar pacientes com depressão, ansiedade e outros transtornos.

Vale lembrar que os jogos terapêuticos são diferentes dos games virtuais. Ainda que sejam online, eles são desenvolvidos por plataformas especializadas para auxiliar seus pacientes a lidar com seus problemas e desafios pessoais e precisam passar pela aprovação do Conselho Federal de Psicologia para serem utilizados na clínica.

# Malefícios <h>
Hoje, estamos rodeados por aparelhos dos mais diversos tipos. São computadores, celulares, tablets, internet, SmartTVs, entre outros que tornam nossas vidas muito mais fáceis. Por outro lado, sabemos que o uso constante destas ferramentas causam danos físicos e psíquicos, como LER (Lesão por Esforço Repetitivo), estresse, varizes, problemas de visão e até mesmo exclusão social.

Até mesmo a obesidade e o sedentarismo são fatores muito ligados ao aumento da tecnologia por conta das mudanças em nossos hábitos que esse novo mundo nos apresentou. Como as pessoas passam a maior parte de seu tempo sentadas, já que hoje é possível até mesmo fazer compras, pedir comida ou solicitar um transporte online, não há mais a necessidade de ir até lojas, restaurantes ou a um ponto de táxi. Além disso, passarmos mais tempo nas redes sociais do que na presença física de nossos familiares, amigos ou de pessoas com quem podemos iniciar algum laço. Isso sem contar o trabalho, que para muitas pessoas é em frente ao computador.

![](https://idec.org.br/sites/default/files/artigos/influencia-digital-social-media.jpg)



# Referências <h>

<https://www.psicologia.pt/artigos/ver_artigo_licenciatura.php?codigo=TL0002&area=d8>. Acesso em: 9 jun 2022.
<https://www.portalopiniaopublica.com.br/ler-coluna/182/os-beneficios-e-maleficios-da-tecnologia-na-sociedade.html>. Acesso em: 10 jun 2022.
<https://www.conteudoinboundmarketing.com.br/beneficios-da-tecnologia/>. Acesso em: 12 jun 2022.
