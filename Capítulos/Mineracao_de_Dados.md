# Mineração de Dados <h>



Com o advento e desenvolvimento das tecnologias e do mundo da informação, os dados se tornaram cada vez mais importantes levando a impulsão do Data Mining.
O conceito se baseia em pesquisar e analisar grandes quantidades de dados, sejam eles pessoais, empresariais, monetários, e com isso criar padrões e tendências, fazendo assim com que se torne um produto de valor imensurável no mercado mundial.

Apesar do que parece, esse conceito surgiu bem antes dos computadores. Segundo pesquisas as primeiras utilizações com essa base foram pelo Teorema de Bayes em 1763 e a descoberta de Análise e Regressão em 1805. Além de posteriormente, com a Máquina de Turing Universal em 1936 até o surgimento dos primeiros computadores.

Atualmente, existe uma grande discussão por conta do atual estado dessa questão. Chegou-se a um ponto onde as próprias pessoas se tornaram um produto para grandes empresas e até as conversas pessoais se tornaram dados a serem utilizados para manipular o consumidor. Essa questão é muito bem retratada no documentário O Dilema das Redes, da Netflix.

Apesar de tudo, essa tecnologia é de primordial importância para o desenvolvimento do mundo. Além do fato de que suas possibilidades de aplicações são inimagináveis.

https://www.youtube.com/watch?v=hEFFCKxYbKM




What Its Data Mining? A Begginer’s Guide. Rutgers Bootcamp, 2022.
Orlowski, Jeff. O Dilema das Redes. Netflix, 2020.
Mineração de Dados. Wikipedia.
