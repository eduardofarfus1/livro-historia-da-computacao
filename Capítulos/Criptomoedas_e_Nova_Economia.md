# Criptomoedas e Nova Economia <h>
A engenharia da computação pode mudar os rumos da economia mundial, e o jeito que olhamos para a tecnologia.  

A engenharia de Computação forma profissionais habilitados para o desenvolvimento e planejamento de softwares e hardwares. No mercado de trabalho as empresas buscam esses profissionais para desenvolver e organizar sistemas atrelados as novas tecnologias.  

A Engenharia da computação e de software é envolvida diretamente a uma revolução tecnológica e econômica, tendo um dos principais pontos as criptomoedas, consideradas um novo dinheiro e que tem anunciado valores absurdos nas bolsas de valores, contendo também alguns mistérios. O bitcoin, por exemplo, é a mais conhecida criptomoeda e surgiu em meados de 2008, como uma forma virtual de dinheiro que pode ser usada e transferida sem a ajuda de bancos. Não se sabe até hoje o paradeiro do surgimento desta moeda, mas especulasse que foi um engenheiro, uma pessoa ou grupo altamente capacitado. As criptomoedas são criptografadas em formato blockchain que é uma forma de compartilhamento e imutável que possibilita o processo de registro, transações e rastreamentos ativos em uma rede empresarial fornecendo integridade de dados, eliminando uma possível duplicação de dados e aumentando a segurança. Assim possibilitando guardar os bitcoins em carteiras digitais, as exchanges, que funciona como armazenador e um intermediador entre vendedores e compradores desses criptoativos digitais, fornecendo uma prática segura e dentro da lei.  
# O engenheiro e o ramo criptoativo <h>

Um profissional da área da computação subsequentemente ao decorrer dos anos de ensino e mercado ele conhece os funcionamentos de sistemas informatizados, entendendo de software e hardware, banco de dados, aplicações e investimento, ele consegue possibilidade de trabalhar com as blockchain, exchanges, criptomoedas e outras áreas do desenvolvimento e aplicação desse ramo cripto.  

Existem uma série de criptomoedas inventadas e implementadas no ramo, além do principal retratado que é o bitcoin(BTC), como o Ethereum(ETH), o Litecoin(LTC), Dash(DASH), HEX(HEX), apeCoin(APE), entre outras que estão disponíveis em um mercado promissor econômico. 

E a computação é o principal ponto, a palavra-chave quando pensamos nas novas tecnologia e nesse o ramo da economia. Entender e observar esse sistema de criptografia é essencial para se obter uma dinâmica entre a área computacional e econômica, para o seu funcionamento, e alguns profissionais estão nesse ramo para programar e analisar a dinâmica de subidas e descidas das bolsas de valores, que estão sendo disputados por muitos em um mercado qualificado.  

A muitas teorias de quem programou e lançou o primeiro criptoaditivo, o bitcoin especulasse os internautas que foi um pseudônimo chamado Satoshi Nakamoto, não se obtém provas de seu paradeiro. O interessante é que a chegada da moeda digital coincidiu com uma das mais graves crises econômicas da história, iniciada nos Estados Unidos, em 2008, o mercado e as instituições financeiras estavam instáveis e com dados desconfiantes. E a cibermoeda teve a principal ideia de autonomia do sistema financeiro formal, de não depender de um monopólio de bancos de dados e a falta de transparência que todo o sistema demostrava na época, tendo sua forma de transação imediata para qualquer lugar do mundo e um potencial de rentabilidade e fácil investimento. Porem toda via como tudo tem seus riscos, tem que se tomar cuidados com golpes e fraudes tanto como uma regulamentação por ser o propósito de novo dinheiro.  

Todo esse ramo de criptoativos digitais envolvendo a engenharia de computação e a economia tem seus prós e contras, muitas cautelas devem ser aplicadas, e a habilidade do engenheiro da computação é essencial no desenvolvimento de sistemas de cibersegurança para garantir a proteção e desenvolvimento desse processo da nova economia de criptos. Muito se investe nesse ramo, como, por exemplo, empreendedor e filantropo bilionário Elon Musk que investiu US$ 1,5 bi em bitcoin, e pessoas como pequenos investidores no cotidiano aplicam uma quantia considerável nesses criptoativos precisando assim de uma base segura e confiável ainda maior, tendo assim que ter profissionais altamente capacitados no ramo de software e hardware em criptoativos.  

A tecnologia vai muito além de aplicar esses ativos e mexer com dinheiro, mas nesse aspecto de cripto, conseguiram transformar e dar um ponto inicial a uma nova economia, quando a tecnólogia transformou o próprio dinheiro em tecnólogia. Tudo pode mudar, é uma variável, mas que tudo indica que a tecnologia a área de computação e a economia não vão parar por aí e se utilizado de maneira ética e consciente pode ajudar em um prol a área e a sociedade. O pensador Stephen Covey retrata "A tecnologia vai reinventar o negócio, mas as relações humanas continuarão a ser a chave para o sucesso". 

# Referências <h>


COUTINHO, Dani – Jornalista e publicitária. “A Engenharia da Computação, as Criptomedas e a Nova Economia”. Tecnologia. ASCOM - Inatel, 26 abril 2022. Disponível em: https://inatel.br/blog/tecnologia/273-a-engenharia-da-computacao-as-criptomoedas-e-a-nova-economia. 

 

MOSS. “Afinal de contas: o que são exchanges?”. Tecnologia. MOSS 2022. Disponível em: https://moss.earth/pt-br/o-que-sao-exchanges/. 

 

IBM. “O que é a tecnologia blockchain?”. Tecnologia. IBM 2022. Disponível em: https://www.ibm.com/br-pt/topics/what-is-blockchain. 

 

Guia da Carreira. “Engenharia da Computação: profissão e mercado”. Tecnologia. Engenharia. Profissão. Disponível em:https://www.guiadacarreira.com.br/guia-das-profissoes/engenharia-computacao/. 

 

By Time Rico. “Criptomoedas: o que são, como funcionam e como investir?”. Investimento. Tecnologia. RICONNECT, agosto 26, 2021.  